const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

//Middlewares
//For Allow-Cross-Origin
app.use(cors());
app.use(bodyParser.json());

//Import Routes
const postRoute = require('./routes/posts');

app.use('/posts', postRoute);

//ROUTES
app.get('/', (req, res) => {
    res.send({ code: 200});
})

//Connect to DB
mongoose.connect(
    process.env.DB_CONNECTION,
    { useNewUrlParser: true },
    () => console.log('Connected to DB')
);

//LISTEN
app.listen(3000);
