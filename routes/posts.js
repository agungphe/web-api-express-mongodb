const express = require('express');
const router = express.Router();
const Post = require('../models/Post');

//Get All Tyhe Posts
router.get('/', async (req, res) => {
    // res.send({ code: 200, msg: 'POSTS' });
    try {
        const posts = await Post.find();
        res.json(posts);
    } catch(err) {
        res.json({message: err});
    }
})

//Submit A Post
router.post('/', async (req, res) => {
    // console.log(req.body);
    // res.send({ code: 200, msg: 'POSTS', data: req.body });
    const post = new Post({
        title: req.body.title,
        description: req.body.description
    });

    try {
        const savedPost = await post.save();
        res.json(savedPost);
    } catch(err) {
        res.json({message: err});
    }

    // post.save()
    //     .then(data => {
    //         res.json(data)
    //     })
    //     .catch(err => {
    //         res.json({ message: err })
    //     })
})

//Specific Post
router.get('/:postId', async (req, res) => {
    // console.log(req.params.postId);
    try {
        const post = await Post.findById(req.params.postId);
        res.json(post);
    } catch(err) {
        res.json({ message: err });
    }
})

//Delete Post
router.delete('/:postId', async (req, res) => {
    try {
        const removedPost = await Post.remove({_id: req.params.postId});
        res.json(removedPost);
    } catch(err) {
        res.json({ message: err });
    }
})

//Update A Post
router.patch('/:postId', async (req, res) => {
    try {
        const updatedPost = await Post.updateOne({_id: req.params.postId }, 
            { $set: { 
                title: req.body.title, 
                description: req.body.description 
            }});
        res.json(updatedPost);
    } catch(err) {
        res.json({ message: err });
    }
})

module.exports = router;